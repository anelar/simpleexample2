# Modul for REST-api

Dette modulen inneholder REST-api for [simpleexample2](../README.md).

## REST-api-et

Et REST-API er på en måte et objekt (eller sett med objekter) som leses og/eller kalles ved å bruke HTTP-forespørsler (HTTP-URL-er). Mottak av forespørslen håndteres av en HTTP-server, og så "omformes" forespørslen til et kall på en tilhørende metode i et "REST-tjeneste"-objekt.

Denne modulen inneholder bare logikken til "REST-tjeneste"-objektet (**LatLongsService**), mens oppsettet av serveren ligger i [restserver-modulen](../restserver/README.md). En slik oppdeling gjør løsningen ryddigere (selv om det øker antall moduler).

REST-API-et vårt er programmert iht. [JAX-RS-standarden](https://en.wikipedia.org/wiki/Java_API_for_RESTful_Web_Services), som lar en angi koblingen mellom HTTP og metodene på REST-tjeneste-objektet vha. Java-annotasjoner. Tjenesteobjektet trenger faktisk ikke implementere noe spesifikt grensesnitt eller arve fra noen spesifikk superklasse, slik en servlet må. F.eks. angir @Path-annotasjonen på **LatLongsService**-klassen at vår tjeneste er knyttet til **latLongs**-prefikset. Altså vil vår tjeneste aktiveres ved å angi **latLongs** først i stien til URL-en (men bak stien til serveren). Anta f.eks. at stien til serveren er "http://min-server:8080/". Da vil stien til vår tjeneste være "http://min-server:8080/latLongs".

Som nevnt er JAX-RS *annotasjonsdrevet*. Det betyr at Java-annotasjoner ved hver metode angir detaljer ved forespørslen som trigger metoden. F.eks. vil en metode annotert med **@GET** bare kalles når HTTP-metoden er "GET". En kan bruke **@Path** for å angi et påkrevd sti-segment og **@PathParam** for å bruke dette sti-segmentet som argument til metoden.

Hvis metoden bruker ekstra data sendt i tillegg til URL-en, så angis formatet med **@Consumes**-annotasjonen. Tilsvarende angis kodingen av resultatet med **@Produces**-annotasjonen. Vi bruker json-kodete data, så derfor bruker vi **MediaType.APPLICATION_JSON** (konstant for "application/json") som argument for begge disse.

Vårt REST-API gir tilgang til *ett* **LatLongs**-objekt, som refereres til av **latLongs**-felter. **@Inject**-annotasjonen er vesentlig, den signaliserer at feltet (kan/skal) settes "automagisk" *utenifra* til et objekt av den angitte typen. Hvordan dette skjer styres av web-serveren som tjenesteklassen vår kjører på, se [restserver-modulen](../restserver/README.md). Slik "injeksjon" er en vanlig teknikk for å gjøre en klasse mer uavhengig av sammenhengen den inngår i.

REST-API-et tilbyr fem metoder:

* lese innholdet (alle **LatLong**-objektene) - GET til **tjeneste-URL**
* lese innholdet til et spesifikt **LatLong**-element (angitt med posisjonen **num**) - GET (**@GET**) til **tjeneste-URL/num** (**@Path** og **@PathParam**)
* legge til LatLongs-objekter - POST (**@POST**) av json-kodet LatLong-array til **tjeneste-URL**
* endre LatLong-objekt i spesifikk posisjon - PUT (**@PUT**) av json-kodet LatLong-objekt til **tjeneste-URL/num** (**@Path** og **@PathParam**)
* fjerne LatLong-objekt i spesifikk posisjon - DELETE (**@DELETE**) til **tjeneste-URL/num** (**@Path** og **@PathParam**)

Merk at navnet på implementasjonsmetoden i **LatLongsService**-klassen ikke spiller noen rolle, det er annotasjonene som er viktige.

Her er et eksempel på forløp, hvor det antas at **LatLongs**-objektet er tomt i starten:

```plantuml
client -> LatLongsService: GET /latLongs
LatLongsService --> client: [ ]
client -> LatLongsService: POST /latLongs ~[[63.1, 11.2], [63.2, 11.0]]
LatLongsService --> client: 0
client -> LatLongsService: GET /latLongs/1
LatLongsService --> client: { "latitude": 63.2, "longitude": 11.0 }
client -> LatLongsService: PUT /latLongs/0 { "latitude": 64.0, "longitude": 9.1 }
LatLongsService --> client: 0
client -> LatLongsService: GET /latLongs
LatLongsService --> client: [ { "latitude": 64.0, "longitude": 9.1 }, { "latitude": 63.2, "longitude": 11.0 } ]
```

Først er altså **LatLongs**-objektet er tomt. Det legges så til to **LatLong**-objekter, som havner i posisjon 0, før **LatLong**-objekter i posisjon 1 hentes. Så endres **LatLong**-objektet i posisjon 0, før alle hentes. Legg merke til at en json-*array* med to tall, f.eks. **[63.1, 11.2]**, kan deserialiseres (leses) som et **LatLong**-objekt, men at det serialiseres (skrives) som et json-*objekt*, f.eks. **{ "latitude": 64.0, "longitude": 9.1 }**.

## JSON-koding av data

Når vi bruker **@Consumes** og **@Produces** med **MediaType.APPLICATION_JSON** som argument, så må vi også konfigurere tjenesten slik at kodingen av json-data bruker vår logikk, altså den som er programmert i [core-modulen](../core/README.md). Ellers kan det blir forskjell på logikken i JavaFX-app-en som sender forespørsler og og mottar svar, og REST-tjenesten, som mottar forespørsel og sender svar. Konfigureringen gjøres i **LatLongObjectMapperProvider**-klassen. Det er ikke så mye å si om den klassen, annet enn at den bruker den samme **LatLongsModule**-klassen fra core-modulen, som JavaFX-app-en bruker.

## Bygging med Gradle

REST-API-koden er satt opp som et prosjekt av typen **java-library**, siden det ikke er noe selvstendig program. Vi har avhengighet til JSON-biblioteket Jackson og JAX-RS-standarden. Vi trenger imidlertid ikke noen avhengighet til en implementasjon av denne standarden, den "byrden" dyttes over på [restserver-modulen](../restserver/README.md).
