package simpleex.ui;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import simpleex.core.LatLong;

public class FxAppTest extends AbstractFxAppTest {

  @Override
  protected URL getFxmlResource() {
    return getClass().getResource("FxApp.fxml");
  }

  private List<LatLong> latLongList;
  private LatLongsDataAccess dataAccess;

  @Override
  protected LatLongsDataAccess getDataAccess() {
    return dataAccess;
  }

  @Override
  protected void setUpLatLongsDataAccess() {
    // test data
    latLongList = new ArrayList<>(List.of(new LatLong(63.1, 11.2), new LatLong(63.2, 11.0)));
    // "mocked" (faked) LatLongsDataAccess object with very specific and limited behavior
    dataAccess = mock(LatLongsDataAccess.class);
    // get nth LatLong object
    when(dataAccess.getLatLong(anyInt()))
    .then(invocation -> latLongList.get(invocation.getArgument(0)));
    // get the LatLong objects
    when(dataAccess.getAllLatLongs()).then(invocation -> new ArrayList<>(latLongList));
    // add center
    when(dataAccess.addLatLong(any(LatLong.class))).then(invocation -> {
      final int size = latLongList.size();
      latLongList.add(invocation.getArgument(0, LatLong.class));
      return size;
    });
  }
}
