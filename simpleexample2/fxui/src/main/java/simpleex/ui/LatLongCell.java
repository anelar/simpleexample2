package simpleex.ui;

import javafx.scene.control.ListCell;
import javafx.scene.layout.Region;
import simpleex.core.LatLong;

/**
 * Renderer for a listview item containg a LatLong object.
 *
 * @author Adrian Stoica
 *
 */
public class LatLongCell extends ListCell<LatLong> {
  
  @Override
  public void updateItem(LatLong location, boolean empty) {
    super.updateItem(location, empty);
    if (empty || location == null) {
      setText(null);
      setGraphic(null);
      setPrefHeight(30.0);
    } else {
      LatLongCellController latLongCellController = new LatLongCellController(this);
      latLongCellController.setLatLong(location);
      setGraphic(latLongCellController.getCellView(this.isSelected()));
      setPrefHeight(Region.USE_COMPUTED_SIZE);
    }
  }
}
